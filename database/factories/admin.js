const faker = require(`faker`)
const { Admin } = require(`../../src/models`)

module.exports = {
  make(data) {
    return {
      name: faker.name.findName(),
      email: `${faker.random.alphaNumeric(6)}1@${faker.random.alphaNumeric(4)}.com`,
      password: faker.random.alphaNumeric(8),
      ...data
    }
  },
  async create(data) {
    const adminData = {
      name: faker.name.findName(),
      email: `${faker.random.alphaNumeric(8)}2@${faker.random.alphaNumeric(4)}.com`,
      password: faker.random.alphaNumeric(8)
    }
    const admin = await Admin.create({ ...adminData, ...data })
    let result = {
      data: adminData,
      admin
    }
    if (!admin) result = await this.create(data)
    return result
  }
}
