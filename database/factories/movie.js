const faker = require(`faker`)
const { Movie } = require(`../../src/models`)

module.exports = {
  make(data) {
    return {
      name: faker.name.title(),
      director: faker.name.findName(),
      gender: faker.random.word(),
      ...data
    }
  },
  async create(data) {
    const movieData = {
      name: faker.name.title(),
      director: faker.name.findName(),
      gender: faker.random.word()
    }
    const movie = await Movie.create({ ...movieData, ...data })
    let result = {
      data: movieData,
      movie
    }
    if (!movie) result = await this.create(data)
    return result
  }
}
