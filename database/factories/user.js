const faker = require(`faker`)
const { User } = require(`../../src/models`)

module.exports = {
  make(data) {
    return {
      name: faker.name.findName(),
      email: `${faker.random.alphaNumeric(6)}1@${faker.random.alphaNumeric(4)}.com`,
      password: faker.random.alphaNumeric(8),
      ...data
    }
  },
  async create(data) {
    const userData = {
      name: faker.name.findName(),
      email: `${faker.random.alphaNumeric(8)}2@${faker.random.alphaNumeric(4)}.com`,
      password: faker.random.alphaNumeric(8)
    }
    const user = await User.create({ ...userData, ...data })
    let result = {
      data: userData,
      user
    }
    if (!user) result = await this.create(data)
    return result
  }
}
