const mediators = require('../../mediators/admin/me')

module.exports = {
  async show(req, res) {
    const { status, data } = await mediators.show(req.admin)
    res.status(status).json(data)
  },

  async edit(req, res) {
    const { status, data } = await mediators.edit(req.admin, req.body)
    res.status(status).json(data)
  },

  async deactivate(req, res) {
    const { status, data } = await mediators.deactivate(req.admin)
    res.status(status).json(data)
  }
}
