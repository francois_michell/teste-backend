const mediators = require('../../mediators/admin/movie')

module.exports = {
  async store(req, res) {
    const { status, data } = await mediators.store(req.body)
    res.status(status).json(data)
  },

  async all(req, res) {
    const { status, data } = await mediators.all(req.query)
    res.status(status).json(data)
  },

  async find(req, res) {
    const { status, data } = await mediators.find(req.params)
    res.status(status).json(data)
  }
}
