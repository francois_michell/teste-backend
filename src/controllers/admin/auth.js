const mediators = require('../../mediators/admin/auth')

module.exports = {
  async store(req, res) {
    const { status, data } = await mediators.store(req.body)
    res.status(status).json(data)
  },

  async login(req, res) {
    const { status, data } = await mediators.login(req.body)
    res.status(status).json(data)
  }
}
