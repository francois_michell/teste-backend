const mediators = require('../../mediators/user/movie')

module.exports = {
  async all(req, res) {
    const { status, data } = await mediators.all(req.query)
    res.status(status).json(data)
  },

  async find(req, res) {
    const { status, data } = await mediators.find(req.params)
    res.status(status).json(data)
  },

  async rate(req, res) {
    const { status, data } = await mediators.rate(req.user, { ...req.params, ...req.body })
    res.status(status).json(data)
  }
}
