const mediators = require('../../mediators/user/me')

module.exports = {
  async show(req, res) {
    const { status, data } = await mediators.show(req.user)
    res.status(status).json(data)
  },

  async edit(req, res) {
    const { status, data } = await mediators.edit(req.user, req.body)
    res.status(status).json(data)
  },

  async deactivate(req, res) {
    const { status, data } = await mediators.deactivate(req.user)
    res.status(status).json(data)
  }
}
