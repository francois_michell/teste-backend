const { Movie, MovieRate, User, Actor } = require(`../../models`)

module.exports = async (id) =>
  Movie.findOne({
    where: { id },
    include: [
      {
        model: MovieRate,
        attributes: ['rate'],
        include: [{ model: User, attributes: ['id', 'name'] }]
      },
      {
        model: Actor,
        attributes: ['name']
      }
    ]
  })
