const { Op } = require('sequelize')

const { Movie, Actor } = require(`../../models`)

const mountWhereClause = (params) => {
  const whereClause = {}
  if (params.name) whereClause.name = { [Op.iLike]: `%${params.name}%` }
  if (params.director) whereClause.director = { [Op.iLike]: `%${params.director}%` }
  if (params.gender) whereClause.gender = { [Op.iLike]: `%${params.gender}%` }

  return whereClause
}

module.exports = async (page, limit, params) => {
  const query = {
    where: mountWhereClause(params),
    attributes: { exclude: ['updatedAt'] },
    offset: (page - 1) * limit,
    limit
  }

  if (params.actor)
    query.include = {
      model: Actor,
      where: { name: { [Op.iLike]: `%${params.actor}%` } },
      required: true
    }

  let movies = await Movie.findAll(query)
  if (params.actor) {
    movies = movies.map((movie) => {
      delete movie.dataValues.Actors
      return movie.dataValues
    })
  }
  return movies
}
