const EventEmitter = require(`eventemitter2`)
const { Movie } = require(`../models`)

const emitter = new EventEmitter()

emitter.on('Movie::rate', async (movieId) => {
  const movie = await Movie.findByPk(movieId)
  const rates = await movie.getMovieRates({ attributes: ['rate'], raw: true })
  const average = rates.reduce((sum, el) => sum + +el.rate, 0) / rates.length
  movie.rate = average
  await movie.save()
})

module.exports = emitter
