const bodyParser = require('body-parser')
const helmet = require('helmet')
const express = require('express')
const fs = require('fs')
const path = require('path')

require('dotenv').config({
  path: process.env.NODE_ENV === 'test' ? '.env.testing' : '.env'
})

const app = express()

app.use(helmet())
app.use(bodyParser.json())

const routePath = path.join(__dirname, '/routes')

fs.readdirSync(routePath).map((namespace) =>
  fs
    .readdirSync(path.join(routePath, namespace))
    .map((routeFile) =>
      app.use(
        `/${namespace}/${routeFile.slice(0, -3)}`,
        require(`./routes/${namespace}/${routeFile.slice(0, -3)}`)
      )
    )
)

app.all('/', (_, res) => {
  res.status(200).json('Bem vinda(o) à api de testes')
})

app.all('*', (_, res) => {
  res.status(404).json({
    code: 'SOMETHING_WENT_WRONG',
    message: 'O que você procurava não foi encontrado'
  })
})
if (process.env.NODE_ENV !== 'test')
  app.listen(process.env.PORT, () => {
    console.log(`Server started on port ${process.env.PORT}`)
  })

module.exports = app
