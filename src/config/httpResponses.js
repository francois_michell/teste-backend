module.exports = {
  ok: (data) => ({ status: 200, data: { data } }),
  created: (data) => ({ status: 201, data: { data } }),
  badRequest: (message) => ({ status: 400, data: { code: 'ERROR', message } }),
  notFound: () => ({
    status: 404,
    data: { code: 'NOT_FOUND', message: 'O que você procurava não foi encontrado' }
  }),
  unprocessableEntity: (message) => ({
    status: 422,
    data: { code: 'SOMETHING_WENT_WRONG', message: message || 'Seu pedido não pôde ser processado' }
  })
}
