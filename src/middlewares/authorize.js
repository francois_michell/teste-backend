/* eslint-disable consistent-return */
const jwt = require(`jsonwebtoken`)
const { User, Admin } = require(`../models`)

module.exports = (req, res, next) => {
  if (!req.headers.authorization)
    return res
      .status(401)
      .json({ code: 'SOMETHING_WENT_WRONG', message: 'Não foi possível autenticar você' })

  const isAdmin = req.originalUrl.split(`/`)[1] === `admin`

  const token = req.headers.authorization.split(` `)[1]
  const secret = `${process.env.SECRET}${isAdmin ? `ADMIN` : `USER`}`

  if (!token)
    return res
      .status(401)
      .json({ code: 'SOMETHING_WENT_WRONG', message: 'Não foi possível autenticar você' })

  jwt.verify(token, secret, async (err, decoded) => {
    if (err) {
      return res
        .status(500)
        .json({ code: 'SOMETHING_WENT_WRONG', message: 'Falha de autenticação.' })
    }

    if (isAdmin) {
      const admin = await Admin.findOne({
        where: { id: decoded.id - +process.env.JWTSALT, deletedAt: null },
        attributes: { exclude: ['password', 'updatedAt', 'deletedAt'] }
      })
      if (!admin)
        return res
          .status(500)
          .json({ code: 'SOMETHING_WENT_WRONG', message: 'Falha de autenticação.' })

      req.admin = admin
    } else {
      const user = await User.findOne({
        where: { id: decoded.id - +process.env.JWTSALT, deletedAt: null },
        attributes: { exclude: ['password', 'updatedAt', 'deletedAt'] }
      })
      if (!user)
        return res
          .status(500)
          .json({ code: 'SOMETHING_WENT_WRONG', message: 'Falha de autenticação.' })

      req.user = user
    }
    next()
  })
}
