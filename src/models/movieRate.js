const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  class MovieRate extends Model {
    static associate(models) {
      this.belongsTo(models.User, { foreignKey: 'userId' })
      this.belongsTo(models.Movie, { foreignKey: 'movieId' })
    }
  }
  MovieRate.init(
    {
      rate: DataTypes.INTEGER,
      movieId: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false
      }
    },
    {
      sequelize,
      modelName: 'MovieRate'
    }
  )
  return MovieRate
}
