const { Model } = require('sequelize')
const bcrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  class Admin extends Model {
    authenticate(password) {
      return bcrypt.compare(password, this.password)
    }
  }
  Admin.init(
    {
      name: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      deletedAt: DataTypes.DATE
    },
    {
      sequelize,
      modelName: 'Admin',
      paranoid: true
    }
  )

  Admin.beforeSave(async (admin) => {
    if (admin._changed.has('password')) {
      admin.password = await bcrypt.hash(admin.password, +process.env.SALT_FACTOR)
    }
  })

  return Admin
}
