const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  class Movie extends Model {
    static associate(models) {
      this.hasMany(models.MovieRate, { foreignKey: 'movieId' })
      this.belongsToMany(models.Actor, { through: 'ActorMovies' })
    }
  }
  Movie.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      gender: {
        type: DataTypes.STRING,
        allowNull: false
      },
      director: {
        type: DataTypes.STRING,
        allowNull: false
      },
      rate: {
        type: DataTypes.DECIMAL(3, 2),
        defaultValue: 4,
        allowNull: false
      }
    },
    {
      sequelize,
      modelName: 'Movie'
    }
  )
  return Movie
}
