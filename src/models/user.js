const { Model } = require('sequelize')
const bcrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      this.hasMany(models.MovieRate, { foreignKey: 'userId' })
    }

    authenticate(password) {
      return bcrypt.compare(password, this.password)
    }
  }
  User.init(
    {
      name: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      deletedAt: {
        type: DataTypes.DATE
      }
    },
    {
      sequelize,
      modelName: 'User',
      paranoid: true
    }
  )

  User.beforeSave(async (user) => {
    if (user._changed.has('password')) {
      user.password = await bcrypt.hash(user.password, +process.env.SALT_FACTOR)
    }
  })
  return User
}
