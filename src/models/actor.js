const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  class Actor extends Model {
    static associate(models) {
      this.belongsToMany(models.Movie, { through: 'ActorMovies' })
    }
  }
  Actor.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      sequelize,
      modelName: 'Actor'
    }
  )
  return Actor
}
