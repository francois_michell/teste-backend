const fs = require('fs')

const mediators = {}

fs.readdirSync(__dirname)
  .filter((element) => element !== 'index.js')
  .map((element) =>
    Object.assign(mediators, {
      [element.slice(0, -3)]: require(`./${element.slice(0, -3)}`)
    })
  )

module.exports = mediators
