const responses = require(`../../../config/httpResponses`)
const jwt = require(`jsonwebtoken`)
const { User } = require('../../../models')

module.exports = async ({ email = '', password = '' }) => {
  try {
    const user = await User.findOne({
      where: { email, deletedAt: null },
      attributes: { exclude: ['updatedAt', 'deletedAt'] }
    })

    if (!user) throw new Error('Email e/ou senha inválido')
    const authenticated = await user.authenticate(password)
    if (!authenticated) throw new Error('Email e/ou senha inválido')

    const userJsonData = user.toJSON()
    delete userJsonData.password

    const token = jwt.sign(
      { id: +process.env.JWTSALT + userJsonData.id },
      `${process.env.SECRET}USER`,
      {
        expiresIn: 300
      }
    )
    return responses.ok({ user: userJsonData, token })
  } catch (error) {
    return responses.unprocessableEntity(error.message)
  }
}
