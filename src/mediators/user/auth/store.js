const responses = require(`../../../config/httpResponses`)
const { User } = require('../../../models')

module.exports = async ({ name, email, password }) => {
  try {
    const user = await User.create({ name, email, password })
    const userJsonData = user.toJSON()
    delete userJsonData.password
    delete userJsonData.updatedAt
    delete userJsonData.deletedAt

    return responses.created(userJsonData)
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
