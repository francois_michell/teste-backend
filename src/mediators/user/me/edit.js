const responses = require(`../../../config/httpResponses`)

module.exports = async (user, { name, email, password }) => {
  try {
    if (name && name !== '') user.name = name
    if (email && email !== '' && Number.isNaN(+email)) user.email = email
    if (password && password !== '') user.password = password

    await user.save()

    const userJSON = user.toJSON()
    delete userJSON.password
    delete userJSON.updatedAt
    delete userJSON.deletedAt

    return responses.ok(userJSON)
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
