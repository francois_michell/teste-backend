const responses = require(`../../../config/httpResponses`)

module.exports = async (user) => {
  try {
    await user.destroy()
    return responses.ok({ message: 'Usuário desativado com sucesso' })
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
