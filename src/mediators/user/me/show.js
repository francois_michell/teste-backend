const responses = require(`../../../config/httpResponses`)

module.exports = (user) => {
  try {
    return responses.ok(user)
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
