const responses = require(`../../../config/httpResponses`)
const eventEmitter = require(`../../../events`)

module.exports = async (user, { id, rate }) => {
  try {
    if (rate < 0 || rate > 4) return responses.badRequest('Pontuação inválida')
    await user.createMovieRate({ movieId: id, rate })

    eventEmitter.emit(`Movie::rate`, id)

    return responses.created({ message: 'Voto computado com sucesso' })
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
