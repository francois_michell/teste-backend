const responses = require(`../../../config/httpResponses`)
const listMovies = require(`../../../services/movie/listMovies`)

module.exports = async ({ page = 1, limit = 20, ...params }) => {
  try {
    const movies = await listMovies(page, limit, params)
    return responses.ok(movies)
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
