const responses = require(`../../../config/httpResponses`)
const findMovie = require(`../../../services/movie/findMovie`)

module.exports = async ({ id }) => {
  try {
    const movie = await findMovie(id)
    if (!movie) return { status: 200, data: { message: 'Filme não encontrado' } }
    return responses.ok(movie)
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
