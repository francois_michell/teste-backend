const responses = require(`../../../config/httpResponses`)
const { Movie, Actor } = require('../../../models')

module.exports = async ({ name, gender, director, actors }) => {
  try {
    const movie = await Movie.create({ name, gender, director })

    if (actors) {
      const promises = []
      actors.map((actorName) =>
        promises.push(
          Actor.findOrCreate({
            where: { name: actorName },
            defaults: { name: actorName }
          })
        )
      )
      const actorsCreated = await Promise.all(promises).then((result) => result.map((el) => el[0]))
      await movie.addActors(actorsCreated)
    }
    return responses.created({ message: 'Filme adicionado com sucesso' })
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
