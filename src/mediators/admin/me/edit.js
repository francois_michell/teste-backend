const responses = require(`../../../config/httpResponses`)

module.exports = async (admin, { name, email, password }) => {
  try {
    if (name && name !== '') admin.name = name
    if (email && email !== '' && Number.isNaN(+email)) admin.email = email
    if (password && password !== '') admin.password = password

    await admin.save()

    const adminJSON = admin.toJSON()
    delete adminJSON.password
    delete adminJSON.updatedAt
    delete adminJSON.deletedAt

    return responses.ok(adminJSON)
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
