const responses = require(`../../../config/httpResponses`)

module.exports = async (admin) => {
  try {
    await admin.destroy()
    return responses.ok({ message: 'Admin desativado com sucesso' })
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
