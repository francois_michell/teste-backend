const responses = require(`../../../config/httpResponses`)

module.exports = (admin) => {
  try {
    return responses.ok(admin)
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
