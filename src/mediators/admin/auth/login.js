const responses = require(`../../../config/httpResponses`)
const jwt = require(`jsonwebtoken`)
const { Admin } = require('../../../models')

module.exports = async ({ email = '', password = '' }) => {
  try {
    const admin = await Admin.findOne({
      where: { email, deletedAt: null },
      attributes: { exclude: ['updatedAt', 'deletedAt'] }
    })
    if (!admin) throw new Error('Email e/ou senha inválido')
    const authenticated = await admin.authenticate(password)
    if (!authenticated) throw new Error('Email e/ou senha inválido')

    const adminJsonData = admin.toJSON()
    delete adminJsonData.password

    const token = jwt.sign(
      { id: +process.env.JWTSALT + adminJsonData.id },
      `${process.env.SECRET}ADMIN`,
      {
        expiresIn: 300
      }
    )
    return responses.ok({ admin: adminJsonData, token })
  } catch (error) {
    return responses.unprocessableEntity(error.message)
  }
}
