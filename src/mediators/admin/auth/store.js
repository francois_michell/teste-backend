const responses = require(`../../../config/httpResponses`)
const { Admin } = require('../../../models')

module.exports = async ({ name, email, password }) => {
  try {
    const admin = await Admin.create({ name, email, password })
    const adminJsonData = admin.toJSON()
    delete adminJsonData.password
    delete adminJsonData.updatedAt
    delete adminJsonData.deletedAt

    return responses.created(adminJsonData)
  } catch (error) {
    return responses.unprocessableEntity()
  }
}
