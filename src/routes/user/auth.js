const express = require('express')

const Router = express.Router()

const controller = require('../../controllers/user/auth')

Router.post('/', controller.store).post('/login', controller.login)

module.exports = Router
