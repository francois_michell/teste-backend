const express = require('express')

const Router = express.Router()

const controller = require('../../controllers/user/movie')

const authorize = require(`../../middlewares/authorize`)

Router.get('/', authorize, controller.all)
  .get('/:id', authorize, controller.find)
  .post('/:id/rate', authorize, controller.rate)
module.exports = Router
