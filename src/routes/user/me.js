const express = require('express')

const Router = express.Router()

const controller = require('../../controllers/user/me')

const authorize = require(`../../middlewares/authorize`)

Router.get('/', authorize, controller.show)
  .put('/', authorize, controller.edit)
  .delete('/', authorize, controller.deactivate)

module.exports = Router
