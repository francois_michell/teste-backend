const express = require('express')

const Router = express.Router()

const controller = require('../../controllers/admin/movie')

const authorize = require(`../../middlewares/authorize`)

Router.post('/', authorize, controller.store)
  .get('/', authorize, controller.all)
  .get('/:id', authorize, controller.find)
module.exports = Router
