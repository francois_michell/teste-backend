const request = require('supertest')
const app = require('../../../src/app')

const userFactory = require(`../../../database/factories/user`)
const { Movie, MovieRate } = require(`../../../src/models`)

// Todos os filmes recuperados
// Recuperar filmes filtrados por nome
// Recuperar filmes filtrados por diretor
// Recuperar filmes filtrados por genero
// Recuperar filmes filtrados por nome, diretor e genero
// Recuperar filmes filtrados por parametros inexistentes
// Recuperar filmes com limite de 2
// Recuperar terceira página de filmes com limite de 2
// Falha ao recuperar filmes sem enviar jwt

let user
let token

beforeAll(async () => {
  const createdUser = await userFactory.create()
  user = createdUser.data
  await MovieRate.destroy({ truncate: { cascade: true } })
  await Movie.destroy({ truncate: { cascade: true } })

  await Movie.bulkCreate([
    { name: 'O iluminado', director: 'Stanley Kubrick', gender: 'Horror' },
    { name: 'Edward mãos de tesoura', director: 'Tim Burton', gender: 'Comédia' },
    { name: 'Taxi driver', director: 'Martin Scorsese', gender: 'Drama' },
    { name: 'Meia noite em Paris', director: 'Woody Allen', gender: 'Comédia' },
    { name: 'Tubarão', director: 'Steven Spielberg', gender: 'Horror' },
    { name: 'Prenda-me se for capaz', director: 'Steven Spielberg', gender: 'Drama' }
  ])
})

beforeEach(async () => {
  const login = await request(app)
    .post(`/user/auth/login`)
    .send({ email: user.email, password: user.password })
  token = login.body.data.token
})

describe(`/user/movie - Rota de listagem dos filmes`, () => {
  test('Sucesso em retornar a lista com todos os filmes', async () => {
    const response = await request(app).get('/user/movie').set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toBeGreaterThanOrEqual(6)
    expect(response.body.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'O iluminado',
          director: 'Stanley Kubrick',
          gender: 'Horror'
        }),
        expect.objectContaining({
          name: 'Edward mãos de tesoura',
          director: 'Tim Burton',
          gender: 'Comédia'
        }),
        expect.objectContaining({
          name: 'Taxi driver',
          director: 'Martin Scorsese',
          gender: 'Drama'
        }),
        expect.objectContaining({
          name: 'Meia noite em Paris',
          director: 'Woody Allen',
          gender: 'Comédia'
        }),
        expect.objectContaining({
          name: 'Tubarão',
          director: 'Steven Spielberg',
          gender: 'Horror'
        }),
        expect.objectContaining({
          name: 'Prenda-me se for capaz',
          director: 'Steven Spielberg',
          gender: 'Drama'
        })
      ])
    )
  })

  test('Sucesso em retornar a lista com filmes filtrados por nome', async () => {
    const response = await request(app)
      .get('/user/movie?name=me')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toBeGreaterThanOrEqual(2)
    expect(response.body.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'Meia noite em Paris',
          director: 'Woody Allen',
          gender: 'Comédia'
        }),
        expect.objectContaining({
          name: 'Prenda-me se for capaz',
          director: 'Steven Spielberg',
          gender: 'Drama'
        })
      ])
    )
  })

  test('Sucesso em retornar a lista com filmes filtrados por diretor', async () => {
    const response = await request(app)
      .get('/user/movie?director=Steven')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toBeGreaterThanOrEqual(2)
    expect(response.body.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'Tubarão',
          director: 'Steven Spielberg',
          gender: 'Horror'
        }),
        expect.objectContaining({
          name: 'Prenda-me se for capaz',
          director: 'Steven Spielberg',
          gender: 'Drama'
        })
      ])
    )
  })

  test('Sucesso em retornar a lista com filmes filtrados por gênero', async () => {
    const response = await request(app)
      .get('/user/movie?gender=Drama')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toBeGreaterThanOrEqual(2)
    expect(response.body.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'Taxi driver',
          director: 'Martin Scorsese',
          gender: 'Drama'
        }),
        expect.objectContaining({
          name: 'Prenda-me se for capaz',
          director: 'Steven Spielberg',
          gender: 'Drama'
        })
      ])
    )
  })

  test('Sucesso em retornar a lista com filmes filtrados por nome, diretor e gênero', async () => {
    const response = await request(app)
      .get('/user/movie?name=a&director=Spielb&gender=Horror')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toBeGreaterThanOrEqual(1)
    expect(response.body.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'Tubarão',
          director: 'Steven Spielberg',
          gender: 'Horror'
        })
      ])
    )
  })

  test('Sucesso em retornar a lista com filmes filtrados por parâmetros inexistentes', async () => {
    const response = await request(app)
      .get('/user/movie?name=diz&director=ola&gender=mundo')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toEqual(0)
  })

  test('Sucesso em retornar a lista com 2 filmes', async () => {
    const response = await request(app)
      .get('/user/movie?limit=2')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toEqual(2)
    expect(response.body.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'O iluminado',
          director: 'Stanley Kubrick',
          gender: 'Horror'
        }),
        expect.objectContaining({
          name: 'Edward mãos de tesoura',
          director: 'Tim Burton',
          gender: 'Comédia'
        })
      ])
    )
  })

  test('Sucesso em retornar a terceira página da lista com 2 filmes', async () => {
    const response = await request(app)
      .get('/user/movie?page=3&limit=2')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toEqual(2)
    expect(response.body.data).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'Tubarão',
          director: 'Steven Spielberg',
          gender: 'Horror'
        }),
        expect.objectContaining({
          name: 'Prenda-me se for capaz',
          director: 'Steven Spielberg',
          gender: 'Drama'
        })
      ])
    )
  })

  test('Falha ao tentar criar um filme sem enviar o jwt', async () => {
    const response = await request(app).get(`/user/movie`)
    expect(response.status).toBe(401)
    expect(response.body).toEqual({
      code: 'SOMETHING_WENT_WRONG',
      message: 'Não foi possível autenticar você'
    })
  })
})
