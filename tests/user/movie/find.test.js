const request = require('supertest')
const app = require('../../../src/app')

const userFactory = require(`../../../database/factories/user`)
const movieFactory = require(`../../../database/factories/movie`)

// Recuperar filme com sucesso
// Falha ao tentar recuperar filme com id inexistente
// Falha ao recuperar filmes sem enviar jwt

let user
let token

beforeAll(async () => {
  const createdUser = await userFactory.create()
  user = createdUser.data
})

beforeEach(async () => {
  const login = await request(app)
    .post(`/user/auth/login`)
    .send({ email: user.email, password: user.password })
  token = login.body.data.token
})

describe(`/user/movie/:id - Rota de detalhe do filme`, () => {
  test('Sucesso em retornar os detalhes do filme', async () => {
    const newMovie = await movieFactory.create()
    const response = await request(app)
      .get(`/user/movie/${newMovie.movie.id}`)
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data).toMatchObject(newMovie.data)
  })

  test('Falha em tentar retornar os detalhes de um filme inexistente', async () => {
    const newMovie = await movieFactory.create()
    const response = await request(app)
      .get(`/user/movie/${newMovie.movie.id + 1}`)
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body).toStrictEqual({ message: 'Filme não encontrado' })
  })

  test('Falha ao tentar recuperar um filme sem enviar o jwt', async () => {
    const newMovie = await movieFactory.create()
    const response = await request(app).get(`/user/movie/${newMovie.movie.id}`)

    expect(response.status).toBe(401)
    expect(response.body).toEqual({
      code: 'SOMETHING_WENT_WRONG',
      message: 'Não foi possível autenticar você'
    })
  })
})
