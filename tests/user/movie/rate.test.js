const request = require('supertest')
const app = require('../../../src/app')

const userFactory = require(`../../../database/factories/user`)
const movieFactory = require(`../../../database/factories/movie`)

// Avaliar filme com sucesso
// Falha ao tentar avaliar num filme que não existe
// Falha ao tentar avaliar com uma pontuação inválida
// Falha ao recuperar filmes sem enviar jwt

let user
let token

beforeAll(async () => {
  const createdUser = await userFactory.create()
  user = createdUser.data
})

beforeEach(async () => {
  const login = await request(app)
    .post(`/user/auth/login`)
    .send({ email: user.email, password: user.password })
  token = login.body.data.token
})

describe(`/user/movie/:id - Rota de avaliação do filme`, () => {
  test('Sucesso em avaliar filme', async () => {
    const newMovie = await movieFactory.create()
    const response = await request(app)
      .post(`/user/movie/${newMovie.movie.id}/rate`)
      .send({ rate: 3 })
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(201)
    expect(response.body.data).toStrictEqual({ message: 'Voto computado com sucesso' })
  })

  test('Falha em tentar avaliar um filme inexistente', async () => {
    const newMovie = await movieFactory.create()
    const response = await request(app)
      .post(`/user/movie/${newMovie.movie.id + 999}/rate`)
      .send({ rate: 3 })
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(422)
    expect(response.body).toStrictEqual({
      code: 'SOMETHING_WENT_WRONG',
      message: 'Seu pedido não pôde ser processado'
    })
  })

  test('Falha em tentar avaliar um filme com pontuação inválida', async () => {
    const newMovie = await movieFactory.create()
    const response = await request(app)
      .post(`/user/movie/${newMovie.movie.id}/rate`)
      .send({ rate: 30 })
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(400)
    expect(response.body).toStrictEqual({ code: 'ERROR', message: 'Pontuação inválida' })
  })

  test('Falha ao tentar recuperar um filme sem enviar o jwt', async () => {
    const newMovie = await movieFactory.create()
    const response = await request(app)
      .post(`/user/movie/${newMovie.movie.id}/rate`)
      .send({ rate: 3 })

    expect(response.status).toBe(401)
    expect(response.body).toEqual({
      code: 'SOMETHING_WENT_WRONG',
      message: 'Não foi possível autenticar você'
    })
  })
})
