const request = require('supertest')
const app = require('../../../src/app')

const userFactory = require(`../../../database/factories/user`)

const errorMessageJson = {
  code: 'SOMETHING_WENT_WRONG',
  message: 'Não foi possível autenticar você'
}

let user
let token

beforeAll(async () => {
  const createdUser = await userFactory.create()
  user = createdUser.user

  const login = await request(app)
    .post(`/user/auth/login`)
    .send({ email: createdUser.data.email, password: createdUser.data.password })
  token = login.body.data.token
})

// Recuperar dados do usuário com sucesso
// Falha ao recuperar dados sem o jwt

describe(`/user/me - Rota de recuperação de dados do usuário`, () => {
  test('Dados recuperados com sucesso', async () => {
    const response = await request(app).get(`/user/me`).set(`Authorization`, `Bearer ${token}`)
    expect(response.status).toBe(200)
    expect(response.body.data).toMatchObject({
      id: expect.anything(),
      name: user.name,
      email: user.email,
      createdAt: user.createdAt.toISOString()
    })
  })

  test('Falha ao tentar recuperar os dados sem enviar o jwt', async () => {
    const response = await request(app).get(`/user/me`)
    expect(response.status).toBe(401)
    expect(response.body).toEqual(errorMessageJson)
  })
})
