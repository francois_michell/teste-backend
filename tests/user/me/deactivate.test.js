const request = require('supertest')
const app = require('../../../src/app')

const userFactory = require(`../../../database/factories/user`)

const errorMessageJson = {
  code: 'SOMETHING_WENT_WRONG',
  message: 'Não foi possível autenticar você'
}

let token

beforeAll(async () => {
  const createdUser = await userFactory.create()
  const login = await request(app)
    .post(`/user/auth/login`)
    .send({ email: createdUser.data.email, password: createdUser.data.password })
  token = login.body.data.token
})

// Desativar usuário com sucesso
// Falhar ao tentar desativar um usuário já desativado
// Falhar ao tentar desativar um usuário sem o jwt

describe(`/user/me - Rota de desativação de usuário`, () => {
  test('Usuário desativado com sucesso', async () => {
    const response = await request(app).delete(`/user/me`).set(`Authorization`, `Bearer ${token}`)
    expect(response.status).toBe(200)
    expect(response.body.data).toEqual({ message: 'Usuário desativado com sucesso' })
  })

  test('Falha ao tentar desativar um usuário já desativado', async () => {
    const response = await request(app).delete('/user/me').set('Authorization', `Bearer ${token}`)
    expect(response.status).toBe(500)
    expect(response.body).toEqual({
      code: 'SOMETHING_WENT_WRONG',
      message: 'Falha de autenticação.'
    })
  })

  test('Falha ao tentar desativar o usuário sem enviar o jwt', async () => {
    const response = await request(app).get(`/user/me`)
    expect(response.status).toBe(401)
    expect(response.body).toEqual(errorMessageJson)
  })
})
