const request = require('supertest')
const app = require('../../../src/app')

const userFactory = require(`../../../database/factories/user`)

let user
let token

beforeEach(async () => {
  const createdUser = await userFactory.create()
  user = createdUser.user

  const login = await request(app)
    .post(`/user/auth/login`)
    .send({ email: createdUser.data.email, password: createdUser.data.password })
  token = login.body.data.token
})

// Alterar apenas o nome do usuário
// Alterar apenas o email do usuário
// Alterar apenas o password do usuário
// Alterar o nome e email do usuário
// Alterar todos os dados do usuário
// Não alterar nada caso os campos sejam vazios
// Não alterar nada caso nada seja enviado

describe(`/user/me - Rota de edição de dados do usuário`, () => {
  test('Alterar apenas o nome do usuário', async () => {
    const toReplace = userFactory.make()
    const response = await request(app)
      .put(`/user/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({
        name: toReplace.name
      })

    const updatedUser = await user.reload()

    expect(response.status).toBe(200)
    expect(response.body.data).toMatchObject({
      id: user.id,
      name: toReplace.name,
      email: user.email,
      createdAt: user.createdAt.toISOString()
    })
    expect(updatedUser.name).toBe(toReplace.name)
  })

  test('Alterar apenas o email do usuário', async () => {
    const toReplace = userFactory.make()
    const response = await request(app)
      .put(`/user/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({
        email: toReplace.email
      })

    const updatedUser = await user.reload()

    expect(response.status).toBe(200)
    expect(response.body.data).toMatchObject({
      id: user.id,
      name: user.name,
      email: toReplace.email,
      createdAt: user.createdAt.toISOString()
    })
    expect(updatedUser.email).toBe(toReplace.email)
  })

  test('Alterar apenas o password do usuário', async () => {
    const toReplace = userFactory.make()
    const response = await request(app)
      .put(`/user/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({
        password: toReplace.password
      })

    const checkLogin = await request(app)
      .post(`/user/auth/login`)
      .send({ email: user.email, password: toReplace.password })

    expect(response.status).toBe(200)
    expect(response.body.data).toMatchObject({
      id: user.id,
      name: user.name,
      email: user.email,
      createdAt: user.createdAt.toISOString()
    })

    expect(checkLogin.status).toBe(200)
    expect(checkLogin.body.data).toMatchObject({
      user: {
        id: user.id,
        name: user.name,
        email: user.email,
        createdAt: user.createdAt.toISOString()
      },
      token: expect.anything()
    })
  })

  test('Alterar o nome e email do usuário', async () => {
    const toReplace = userFactory.make()
    const response = await request(app)
      .put(`/user/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({
        name: toReplace.name,
        email: toReplace.email
      })

    const updatedUser = await user.reload()

    expect(response.status).toBe(200)
    expect(response.body.data).toMatchObject({
      id: user.id,
      name: toReplace.name,
      email: toReplace.email,
      createdAt: user.createdAt.toISOString()
    })
    expect(updatedUser.name).toBe(toReplace.name)
    expect(updatedUser.email).toBe(toReplace.email)
  })

  test('Alterar todos os dados do usuário', async () => {
    const toReplace = userFactory.make()
    const response = await request(app)
      .put(`/user/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({
        name: toReplace.name,
        email: toReplace.email,
        password: toReplace.password
      })

    const updatedUser = await user.reload()

    const checkLogin = await request(app)
      .post(`/user/auth/login`)
      .send({ email: toReplace.email, password: toReplace.password })

    expect(response.status).toBe(200)
    expect(response.body.data).toMatchObject({
      id: user.id,
      name: toReplace.name,
      email: toReplace.email,
      createdAt: user.createdAt.toISOString()
    })
    expect(updatedUser.name).toBe(toReplace.name)
    expect(updatedUser.email).toBe(toReplace.email)
    expect(checkLogin.status).toBe(200)
    expect(checkLogin.body.data).toMatchObject({
      user: {
        id: user.id,
        name: toReplace.name,
        email: toReplace.email,
        createdAt: user.createdAt.toISOString()
      },
      token: expect.anything()
    })
  })

  test('Não alterar caso os campos estejam vazios', async () => {
    const response = await request(app)
      .put(`/user/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({ name: '', email: '', password: '' })

    const updatedUser = await user.reload()

    expect(response.status).toBe(200)
    expect(response.body.data).toMatchObject({
      id: user.id,
      name: user.name,
      email: user.email,
      createdAt: user.createdAt.toISOString()
    })
    expect(updatedUser.name).toBe(user.name)
    expect(updatedUser.email).toBe(user.email)
  })

  test('Não alterar caso nada seja enviado', async () => {
    const response = await request(app)
      .put(`/user/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send()

    const updatedUser = await user.reload()

    expect(response.status).toBe(200)
    expect(response.body.data).toMatchObject({
      id: user.id,
      name: user.name,
      email: user.email,
      createdAt: user.createdAt.toISOString()
    })
    expect(updatedUser.name).toBe(user.name)
    expect(updatedUser.email).toBe(user.email)
  })

  test('Falha ao tentar alterar os dados sem enviar o jwt', async () => {
    const toReplace = userFactory.make()
    const response = await request(app).put(`/user/me`).send({
      name: toReplace.name,
      email: toReplace.email
    })
    expect(response.status).toBe(401)
    expect(response.body).toEqual({
      code: 'SOMETHING_WENT_WRONG',
      message: 'Não foi possível autenticar você'
    })
  })
})
