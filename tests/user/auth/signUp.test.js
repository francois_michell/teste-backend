const request = require('supertest')
const app = require('../../../src/app')

const userFactory = require(`../../../database/factories/user`)

const errorMessageJson = {
  code: 'SOMETHING_WENT_WRONG',
  message: 'Seu pedido não pôde ser processado'
}

// usuário criado com sucesso
// Falha ao criar usuário com email repetido
// Falha ao criar usuário sem enviar o email
// Falha ao criar usuário sem enviar o nome
// Falha ao criar usuário sem enviar o password
// Falha ao criar usuário sem enviar nada

describe(`/user/auth/ - Rota de criação do usuário`, () => {
  test('Quando o usuário é criado com sucesso', async () => {
    const newUser = userFactory.make()
    const response = await request(app).post(`/user/auth`).send(newUser)
    expect(response.status).toBe(201)
    expect(response.body).toMatchObject({
      data: {
        name: newUser.name,
        email: newUser.email
      }
    })
  })

  test('Falha ao criar usuário com email repetido', async () => {
    const createdUser = await userFactory.create()
    const newUser = userFactory.make({ email: createdUser.data.email })
    const response = await request(app).post(`/user/auth`).send(newUser)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao criar usuário sem enviar o email', async () => {
    const newUser = userFactory.make({ email: undefined })
    const response = await request(app).post(`/user/auth`).send(newUser)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao criar usuário sem enviar o nome', async () => {
    const newUser = userFactory.make({ name: undefined })
    const response = await request(app).post(`/user/auth`).send(newUser)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao criar usuário sem enviar o password', async () => {
    const newUser = userFactory.make({ password: undefined })
    const response = await request(app).post(`/user/auth`).send(newUser)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao criar usuário sem enviar nada', async () => {
    const response = await request(app).post(`/user/auth`).send({})
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })
})
