const request = require('supertest')
const app = require('../../../src/app')

const userFactory = require(`../../../database/factories/user`)

const errorMessageJson = {
  code: 'SOMETHING_WENT_WRONG',
  message: 'Email e/ou senha inválido'
}

// Usuário logado com sucesso
// Falha ao tentar logar sem enviar o email
// Falha ao tentar logar sem enviar o passwor
// Falha ao tentar logar com um email não cadastrado
// Falha ao tentar logar com o password errado
// Falha ao tentar logar sem enviar dados

describe(`/user/auth/login - Rota de login do usuário`, () => {
  test('Usuário logado com sucesso', async () => {
    const createdUser = await userFactory.create()
    const response = await request(app)
      .post(`/user/auth/login`)
      .send({ email: createdUser.data.email, password: createdUser.data.password })
    expect(response.status).toBe(200)
    expect(response.body).toMatchObject({
      data: {
        user: {
          name: createdUser.user.name,
          email: createdUser.user.email
        },
        token: expect.anything()
      }
    })
  })

  test('Falha ao tentar logar sem enviar o email', async () => {
    const createdUser = await userFactory.create()
    const response = await request(app)
      .post(`/user/auth/login`)
      .send({ email: '', password: createdUser.data.password })
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao tentar logar sem enviar o password', async () => {
    const createdUser = await userFactory.create()
    const response = await request(app)
      .post('/user/auth/login')
      .send({ email: createdUser.data.email })
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao tentar logar com um email não cadastrado', async () => {
    const userNotRegistered = userFactory.make()
    const response = await request(app)
      .post('/user/auth/login')
      .send({ email: userNotRegistered.email })
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao tentar logar com o password errado', async () => {
    const createdUser = await userFactory.create()
    const response = await request(app)
      .post('/user/auth/login')
      .send({ email: createdUser.data.email, password: 'password errado' })
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao tentar logar sem enviar dados', async () => {
    const response = await request(app).post('/user/auth/login').send()
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })
})
