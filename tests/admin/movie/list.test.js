const request = require('supertest')
const app = require('../../../src/app')

const adminFactory = require(`../../../database/factories/admin`)
const { Movie, MovieRate } = require(`../../../src/models`)

// Todos os filmes recuperados
// Recuperar filmes filtrados por nome
// Recuperar filmes filtrados por diretor
// Recuperar filmes filtrados por genero
// Recuperar filmes filtrados por nome, diretor e genero
// Recuperar filmes filtrados por parametros inexistentes
// Recuperar filmes com limite de 2
// Recuperar terceira página de filmes com limite de 2
// Falha ao recuperar filmes sem enviar jwt

let admin
let token

beforeAll(async () => {
  const createdAdmin = await adminFactory.create()
  admin = createdAdmin.data

  await MovieRate.destroy({ truncate: { cascade: true } })
  await Movie.destroy({ truncate: { cascade: true } })

  await Movie.bulkCreate([
    { name: 'O iluminado', director: 'Stanley Kubrick', gender: 'Horror' },
    { name: 'Edward mãos de tesoura', director: 'Tim Burton', gender: 'Comédia' },
    { name: 'Taxi driver', director: 'Martin Scorsese', gender: 'Drama' },
    { name: 'Meia noite em Paris', director: 'Woody Allen', gender: 'Comédia' },
    { name: 'Tubarão', director: 'Steven Spielberg', gender: 'Horror' },
    { name: 'Prenda-me se for capaz', director: 'Steven Spielberg', gender: 'Drama' }
  ])
})

beforeEach(async () => {
  const login = await request(app)
    .post(`/admin/auth/login`)
    .send({ email: admin.email, password: admin.password })
  token = login.body.data.token
})

describe(`/admin/movie - Rota de listagem dos filmes`, () => {
  test('Sucesso em retornar a lista com todos os filmes', async () => {
    const response = await request(app).get('/admin/movie').set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toBeGreaterThanOrEqual(6)
    expect(response.body).toEqual({
      data: expect.arrayContaining([
        expect.objectContaining({
          name: 'O iluminado',
          director: 'Stanley Kubrick',
          gender: 'Horror'
        }),
        expect.objectContaining({
          name: 'Edward mãos de tesoura',
          director: 'Tim Burton',
          gender: 'Comédia'
        }),
        expect.objectContaining({
          name: 'Taxi driver',
          director: 'Martin Scorsese',
          gender: 'Drama'
        }),
        expect.objectContaining({
          name: 'Meia noite em Paris',
          director: 'Woody Allen',
          gender: 'Comédia'
        }),
        expect.objectContaining({
          name: 'Tubarão',
          director: 'Steven Spielberg',
          gender: 'Horror'
        }),
        expect.objectContaining({
          name: 'Prenda-me se for capaz',
          director: 'Steven Spielberg',
          gender: 'Drama'
        })
      ])
    })
  })

  test('Sucesso em retornar a lista com filmes filtrados por nome', async () => {
    const response = await request(app)
      .get('/admin/movie?name=me')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toBeGreaterThanOrEqual(2)
    expect(response.body).toEqual({
      data: expect.arrayContaining([
        expect.objectContaining({
          name: 'Meia noite em Paris',
          director: 'Woody Allen',
          gender: 'Comédia'
        }),
        expect.objectContaining({
          name: 'Prenda-me se for capaz',
          director: 'Steven Spielberg',
          gender: 'Drama'
        })
      ])
    })
  })

  test('Sucesso em retornar a lista com filmes filtrados por diretor', async () => {
    const response = await request(app)
      .get('/admin/movie?director=Steven')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toBeGreaterThanOrEqual(2)
    expect(response.body).toEqual({
      data: expect.arrayContaining([
        expect.objectContaining({
          name: 'Tubarão',
          director: 'Steven Spielberg',
          gender: 'Horror'
        }),
        expect.objectContaining({
          name: 'Prenda-me se for capaz',
          director: 'Steven Spielberg',
          gender: 'Drama'
        })
      ])
    })
  })

  test('Sucesso em retornar a lista com filmes filtrados por gênero', async () => {
    const response = await request(app)
      .get('/admin/movie?gender=Drama')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toBeGreaterThanOrEqual(2)
    expect(response.body).toEqual({
      data: expect.arrayContaining([
        expect.objectContaining({
          name: 'Taxi driver',
          director: 'Martin Scorsese',
          gender: 'Drama'
        }),
        expect.objectContaining({
          name: 'Prenda-me se for capaz',
          director: 'Steven Spielberg',
          gender: 'Drama'
        })
      ])
    })
  })

  test('Sucesso em retornar a lista com filmes filtrados por nome, diretor e gênero', async () => {
    const response = await request(app)
      .get('/admin/movie?name=a&director=Spielb&gender=Horror')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toBeGreaterThanOrEqual(1)
    expect(response.body).toEqual({
      data: expect.arrayContaining([
        expect.objectContaining({
          name: 'Tubarão',
          director: 'Steven Spielberg',
          gender: 'Horror'
        })
      ])
    })
  })

  test('Sucesso em retornar a lista com filmes filtrados por parâmetros inexistentes', async () => {
    const response = await request(app)
      .get('/admin/movie?name=diz&director=ola&gender=mundo')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toEqual(0)
  })

  test('Sucesso em retornar a lista com 2 filmes', async () => {
    const response = await request(app)
      .get('/admin/movie?limit=2')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toEqual(2)
    expect(response.body).toEqual({
      data: expect.arrayContaining([
        expect.objectContaining({
          name: 'O iluminado',
          director: 'Stanley Kubrick',
          gender: 'Horror'
        }),
        expect.objectContaining({
          name: 'Edward mãos de tesoura',
          director: 'Tim Burton',
          gender: 'Comédia'
        })
      ])
    })
  })

  test('Sucesso em retornar a terceira página da lista com 2 filmes', async () => {
    const response = await request(app)
      .get('/admin/movie?page=3&limit=2')
      .set(`Authorization`, `Bearer ${token}`)

    expect(response.status).toBe(200)
    expect(response.body.data.length).toEqual(2)
    expect(response.body).toEqual({
      data: expect.arrayContaining([
        expect.objectContaining({
          name: 'Tubarão',
          director: 'Steven Spielberg',
          gender: 'Horror'
        }),
        expect.objectContaining({
          name: 'Prenda-me se for capaz',
          director: 'Steven Spielberg',
          gender: 'Drama'
        })
      ])
    })
  })

  test('Falha ao tentar criar um filme sem enviar o jwt', async () => {
    const response = await request(app).get(`/admin/movie`)
    expect(response.status).toBe(401)
    expect(response.body).toEqual({
      code: 'SOMETHING_WENT_WRONG',
      message: 'Não foi possível autenticar você'
    })
  })
})
