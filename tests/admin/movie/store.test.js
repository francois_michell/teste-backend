const request = require('supertest')
const app = require('../../../src/app')

const adminFactory = require(`../../../database/factories/admin`)
const movieFactory = require(`../../../database/factories/movie`)

const errorMessageJson = {
  code: 'SOMETHING_WENT_WRONG',
  message: 'Seu pedido não pôde ser processado'
}

// Filme adicionado com sucesso
// Falha ao tentar adicionar filme sem nome
// Falha ao tentar adicionar filme sem diretor
// Falha ao tentar adicionar filme sem genero
// Falha ao tentar adicionar filme sem enviar jwt

let admin
let token

beforeAll(async () => {
  const createdAdmin = await adminFactory.create()
  admin = createdAdmin.data
})

beforeEach(async () => {
  const login = await request(app)
    .post(`/admin/auth/login`)
    .send({ email: admin.email, password: admin.password })
  token = login.body.data.token
})

describe(`/admin/movie - Rota de criação de filme`, () => {
  test('Filme adicionado com sucesso', async () => {
    const newMovie = movieFactory.make()
    const response = await request(app)
      .post(`/admin/movie`)
      .set(`Authorization`, `Bearer ${token}`)
      .send(newMovie)
    expect(response.status).toBe(201)
    expect(response.body).toStrictEqual({ data: { message: 'Filme adicionado com sucesso' } })
  })

  test('Falha ao tentar adicionar filme sem nome', async () => {
    const newMovie = movieFactory.make({ name: undefined })

    const response = await request(app)
      .post(`/admin/movie`)
      .set(`Authorization`, `Bearer ${token}`)
      .send(newMovie)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao tentar adicionar filme sem diretor', async () => {
    const newMovie = movieFactory.make({ director: undefined })

    const response = await request(app)
      .post(`/admin/movie`)
      .set(`Authorization`, `Bearer ${token}`)
      .send(newMovie)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao tentar adicionar filme sem gênero', async () => {
    const newMovie = movieFactory.make({ gender: undefined })

    const response = await request(app)
      .post(`/admin/movie`)
      .set(`Authorization`, `Bearer ${token}`)
      .send(newMovie)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })
  test('Falha ao tentar criar um filme sem enviar o jwt', async () => {
    const newMovie = movieFactory.make()

    const response = await request(app).post(`/admin/movie`).send(newMovie)
    expect(response.status).toBe(401)
    expect(response.body).toEqual({
      code: 'SOMETHING_WENT_WRONG',
      message: 'Não foi possível autenticar você'
    })
  })
})
