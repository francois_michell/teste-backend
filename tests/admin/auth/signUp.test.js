const request = require('supertest')
const app = require('../../../src/app')

const adminFactory = require(`../../../database/factories/admin`)

const errorMessageJson = {
  code: 'SOMETHING_WENT_WRONG',
  message: 'Seu pedido não pôde ser processado'
}

// admin criado com sucesso
// Falha ao criar admin com email repetido
// Falha ao criar admin sem enviar o email
// Falha ao criar admin sem enviar o nome
// Falha ao criar admin sem enviar o password
// Falha ao criar admin sem enviar nada

describe(`/admin/auth/ - Rota de criação do admin`, () => {
  test('Quando o admin é criado com sucesso', async () => {
    const newAdmin = adminFactory.make()
    const response = await request(app).post(`/admin/auth`).send(newAdmin)
    expect(response.status).toBe(201)
    expect(response.body).toMatchObject({
      data: {
        name: newAdmin.name,
        email: newAdmin.email
      }
    })
  })

  test('Falha ao criar admin com email repetido', async () => {
    const createdAdmin = await adminFactory.create()
    const newAdmin = adminFactory.make({ email: createdAdmin.data.email })
    const response = await request(app).post(`/admin/auth`).send(newAdmin)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao criar admin sem enviar o email', async () => {
    const newAdmin = adminFactory.make({ email: undefined })
    const response = await request(app).post(`/admin/auth`).send(newAdmin)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao criar admin sem enviar o nome', async () => {
    const newAdmin = adminFactory.make({ name: undefined })
    const response = await request(app).post(`/admin/auth`).send(newAdmin)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao criar admin sem enviar o password', async () => {
    const newAdmin = adminFactory.make({ password: undefined })
    const response = await request(app).post(`/admin/auth`).send(newAdmin)
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao criar admin sem enviar nada', async () => {
    const response = await request(app).post(`/admin/auth`).send({})
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })
})
