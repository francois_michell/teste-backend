const request = require('supertest')
const app = require('../../../src/app')

const adminFactory = require(`../../../database/factories/admin`)

const errorMessageJson = {
  code: 'SOMETHING_WENT_WRONG',
  message: 'Email e/ou senha inválido'
}

// admin logado com sucesso
// Falha ao tentar logar sem enviar o email
// Falha ao tentar logar sem enviar o passwor
// Falha ao tentar logar com um email não cadastrado
// Falha ao tentar logar com o password errado
// Falha ao tentar logar sem enviar dados

describe(`/admin/auth/login - Rota de login do admin`, () => {
  test('Admin logado com sucesso', async () => {
    const createdAdmin = await adminFactory.create()
    const response = await request(app)
      .post(`/admin/auth/login`)
      .send({ email: createdAdmin.data.email, password: createdAdmin.data.password })
    expect(response.status).toBe(200)
    expect(response.body).toMatchObject({
      data: {
        admin: {
          name: createdAdmin.admin.name,
          email: createdAdmin.admin.email
        },
        token: expect.anything()
      }
    })
  })

  test('Falha ao tentar logar sem enviar o email', async () => {
    const createdAdmin = await adminFactory.create()
    const response = await request(app)
      .post(`/admin/auth/login`)
      .send({ email: '', password: createdAdmin.data.password })
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao tentar logar sem enviar o password', async () => {
    const createdAdmin = await adminFactory.create()
    const response = await request(app)
      .post('/admin/auth/login')
      .send({ email: createdAdmin.data.email })
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao tentar logar com um email não cadastrado', async () => {
    const adminNotRegistered = adminFactory.make()
    const response = await request(app)
      .post('/admin/auth/login')
      .send({ email: adminNotRegistered.email })
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao tentar logar com o password errado', async () => {
    const createdAdmin = await adminFactory.create()
    const response = await request(app)
      .post('/admin/auth/login')
      .send({ email: createdAdmin.data.email, password: 'password errado' })
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })

  test('Falha ao tentar logar sem enviar dados', async () => {
    const response = await request(app).post('/admin/auth/login').send()
    expect(response.status).toBe(422)
    expect(response.body).toEqual(errorMessageJson)
  })
})
