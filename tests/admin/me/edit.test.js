const request = require('supertest')
const app = require('../../../src/app')

const adminFactory = require(`../../../database/factories/admin`)

let admin
let token

beforeEach(async () => {
  const createdAdmin = await adminFactory.create()
  admin = createdAdmin.admin

  const login = await request(app)
    .post(`/admin/auth/login`)
    .send({ email: createdAdmin.data.email, password: createdAdmin.data.password })
  token = login.body.data.token
})

// Alterar apenas o nome do admin
// Alterar apenas o email do admin
// Alterar apenas o password do admin
// Alterar o nome e email do admin
// Alterar todos os dados do admin
// Não alterar nada caso os campos sejam vazios
// Não alterar nada caso nada seja enviado

describe(`/admin/me - Rota de edição de dados do admin`, () => {
  test('Alterar apenas o nome do admin', async () => {
    const toReplace = adminFactory.make()
    const response = await request(app)
      .put(`/admin/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({
        name: toReplace.name
      })

    const updatedAdmin = await admin.reload()

    expect(response.status).toBe(200)
    expect(response.body).toMatchObject({
      data: {
        id: admin.id,
        name: toReplace.name,
        email: admin.email,
        createdAt: admin.createdAt.toISOString()
      }
    })
    expect(updatedAdmin.name).toBe(toReplace.name)
  })

  test('Alterar apenas o email do admin', async () => {
    const toReplace = adminFactory.make()
    const response = await request(app)
      .put(`/admin/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({
        email: toReplace.email
      })

    const updatedAdmin = await admin.reload()

    expect(response.status).toBe(200)
    expect(response.body).toMatchObject({
      data: {
        id: admin.id,
        name: admin.name,
        email: toReplace.email,
        createdAt: admin.createdAt.toISOString()
      }
    })
    expect(updatedAdmin.email).toBe(toReplace.email)
  })

  test('Alterar apenas o password do admin', async () => {
    const toReplace = adminFactory.make()
    const response = await request(app)
      .put(`/admin/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({
        password: toReplace.password
      })

    const checkLogin = await request(app)
      .post(`/admin/auth/login`)
      .send({ email: admin.email, password: toReplace.password })

    expect(response.status).toBe(200)
    expect(response.body).toMatchObject({
      data: {
        id: admin.id,
        name: admin.name,
        email: admin.email,
        createdAt: admin.createdAt.toISOString()
      }
    })

    expect(checkLogin.status).toBe(200)
    expect(checkLogin.body).toMatchObject({
      data: {
        admin: {
          id: admin.id,
          name: admin.name,
          email: admin.email,
          createdAt: admin.createdAt.toISOString()
        },
        token: expect.anything()
      }
    })
  })

  test('Alterar o nome e email do admin', async () => {
    const toReplace = adminFactory.make()
    const response = await request(app)
      .put(`/admin/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({
        name: toReplace.name,
        email: toReplace.email
      })

    const updatedAdmin = await admin.reload()

    expect(response.status).toBe(200)
    expect(response.body).toMatchObject({
      data: {
        id: admin.id,
        name: toReplace.name,
        email: toReplace.email,
        createdAt: admin.createdAt.toISOString()
      }
    })
    expect(updatedAdmin.name).toBe(toReplace.name)
    expect(updatedAdmin.email).toBe(toReplace.email)
  })

  test('Alterar todos os dados do admin', async () => {
    const toReplace = adminFactory.make()
    const response = await request(app)
      .put(`/admin/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({
        name: toReplace.name,
        email: toReplace.email,
        password: toReplace.password
      })

    const updatedAdmin = await admin.reload()

    const checkLogin = await request(app)
      .post(`/admin/auth/login`)
      .send({ email: toReplace.email, password: toReplace.password })

    expect(response.status).toBe(200)
    expect(response.body).toMatchObject({
      data: {
        id: admin.id,
        name: toReplace.name,
        email: toReplace.email,
        createdAt: admin.createdAt.toISOString()
      }
    })
    expect(updatedAdmin.name).toBe(toReplace.name)
    expect(updatedAdmin.email).toBe(toReplace.email)
    expect(checkLogin.status).toBe(200)
    expect(checkLogin.body).toMatchObject({
      data: {
        admin: {
          id: admin.id,
          name: toReplace.name,
          email: toReplace.email,
          createdAt: admin.createdAt.toISOString()
        },
        token: expect.anything()
      }
    })
  })

  test('Não alterar caso os campos estejam vazios', async () => {
    const response = await request(app)
      .put(`/admin/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send({ name: '', email: '', password: '' })

    const updatedAdmin = await admin.reload()

    expect(response.status).toBe(200)
    expect(response.body).toMatchObject({
      data: {
        id: admin.id,
        name: admin.name,
        email: admin.email,
        createdAt: admin.createdAt.toISOString()
      }
    })
    expect(updatedAdmin.name).toBe(admin.name)
    expect(updatedAdmin.email).toBe(admin.email)
  })

  test('Não alterar caso nada seja enviado', async () => {
    const response = await request(app)
      .put(`/admin/me`)
      .set(`Authorization`, `Bearer ${token}`)
      .send()

    const updatedAdmin = await admin.reload()

    expect(response.status).toBe(200)
    expect(response.body).toMatchObject({
      data: {
        id: admin.id,
        name: admin.name,
        email: admin.email,
        createdAt: admin.createdAt.toISOString()
      }
    })
    expect(updatedAdmin.name).toBe(admin.name)
    expect(updatedAdmin.email).toBe(admin.email)
  })

  test('Falha ao tentar alterar os dados sem enviar o jwt', async () => {
    const toReplace = adminFactory.make()
    const response = await request(app).put(`/admin/me`).send({
      name: toReplace.name,
      email: toReplace.email
    })
    expect(response.status).toBe(401)
    expect(response.body).toEqual({
      code: 'SOMETHING_WENT_WRONG',
      message: 'Não foi possível autenticar você'
    })
  })
})
