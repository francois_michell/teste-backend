const request = require('supertest')
const app = require('../../../src/app')

const adminFactory = require(`../../../database/factories/admin`)

const errorMessageJson = {
  code: 'SOMETHING_WENT_WRONG',
  message: 'Não foi possível autenticar você'
}

let token

beforeAll(async () => {
  const createdAdmin = await adminFactory.create()
  const login = await request(app)
    .post(`/admin/auth/login`)
    .send({ email: createdAdmin.data.email, password: createdAdmin.data.password })
  token = login.body.data.token
})

// Desativar admin com sucesso
// Falhar ao tentar desativar um admin já desativado
// Falhar ao tentar desativar um admin sem o jwt

describe(`/admin/me - Rota de desativação de admin`, () => {
  test('Admin desativado com sucesso', async () => {
    const response = await request(app).delete(`/admin/me`).set(`Authorization`, `Bearer ${token}`)
    expect(response.status).toBe(200)
    expect(response.body).toEqual({ data: { message: 'Admin desativado com sucesso' } })
  })

  test('Falha ao tentar desativar um usuário já desativado', async () => {
    const response = await request(app).delete('/admin/me').set('Authorization', `Bearer ${token}`)
    expect(response.status).toBe(500)
    expect(response.body).toEqual({
      code: 'SOMETHING_WENT_WRONG',
      message: 'Falha de autenticação.'
    })
  })

  test('Falha ao tentar desativar o usuário sem enviar o jwt', async () => {
    const response = await request(app).get(`/admin/me`)
    expect(response.status).toBe(401)
    expect(response.body).toEqual(errorMessageJson)
  })
})
