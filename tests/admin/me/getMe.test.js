const request = require('supertest')
const app = require('../../../src/app')

const adminFactory = require(`../../../database/factories/admin`)

const errorMessageJson = {
  code: 'SOMETHING_WENT_WRONG',
  message: 'Não foi possível autenticar você'
}

let admin
let token

beforeAll(async () => {
  const createdAdmin = await adminFactory.create()
  admin = createdAdmin.admin

  const login = await request(app)
    .post(`/admin/auth/login`)
    .send({ email: createdAdmin.data.email, password: createdAdmin.data.password })
  token = login.body.data.token
})

// Recuperar dados do admin com sucesso
// Falha ao recuperar dados sem o jwt

describe(`/admin/me - Rota de recuperação de dados do admin`, () => {
  test('Dados recuperados com sucesso', async () => {
    const response = await request(app).get(`/admin/me`).set(`Authorization`, `Bearer ${token}`)
    expect(response.status).toBe(200)
    expect(response.body).toMatchObject({
      data: {
        id: expect.anything(),
        name: admin.name,
        email: admin.email,
        createdAt: admin.createdAt.toISOString()
      }
    })
  })

  test('Falha ao tentar recuperar os dados sem enviar o jwt', async () => {
    const response = await request(app).get(`/admin/me`)
    expect(response.status).toBe(401)
    expect(response.body).toEqual(errorMessageJson)
  })
})
